FROM node:20.12.2-alpine3.19

RUN apk add --no-cache nginx curl git openssh supervisor bash gettext && \
    rm -f /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf && \
    mkdir -p /var/log/supervisor /etc/supervisor/conf.d/
